package view

import model.engine.Field

abstract class View {
    abstract fun viewField(field: Field)
}
