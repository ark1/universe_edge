package view.lanterna

import model.engine.*

import com.googlecode.lanterna.TextCharacter
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.screen.TerminalScreen
import com.googlecode.lanterna.terminal.*
import view.*
import java.io.Closeable

class LanternaView(private val toChar: Cell.() -> Char) : View(), Closeable {

    private val terminal: Terminal = DefaultTerminalFactory().createTerminal()
    val screen = TerminalScreen(terminal)

    init {
        screen.startScreen()
    }

    override fun viewField(field: Field) {
        screen.clear()
        for (y in 0 until field.h) {
            for (x in 0 until field.w) {
                screen.setCharacter(x, y, TextCharacter(field[Position(x, y)].toChar()))
            }
        }
        screen.refresh()
    }

    override fun close() {
        screen.stopScreen()
    }
}

interface KeyStrokeProcessor<out E : EntityEssence, out P : Performer> {
    fun getPerformer(keyStroke: KeyStroke): EntityPerformer<E, P>?

    companion object {
        inline operator fun <P : Performer, E : EntityEssence> invoke(
                entity: Entity<E>,
                crossinline processKeyStroke: (KeyStroke) -> P?
        ) = object : KeyStrokeProcessor<E, P> {
            override fun getPerformer(keyStroke: KeyStroke) = processKeyStroke(keyStroke)?.let { EntityPerformer(entity, it) }
        }
    }
}

class LanternaPlayer<out E : EntityEssence, out P : Performer>(
    private val view: LanternaView,
    private val keyStrokeProcessor: KeyStrokeProcessor<E, P>
) : Player<E, P> {
    override fun getPerformer() = wrap { keyStrokeProcessor.getPerformer(view.screen.readInput()) }
}
