package view.console

import view.*
import model.engine.*

import java.io.InputStream
import java.io.PrintStream

class ConsoleView(
    input: InputStream = System.`in`,
    private val output: PrintStream = System.out,
    private val toChar: Cell.() -> Char
) : View() {

    val input = input.bufferedReader()

    override fun viewField(field: Field) {
        for (y in 0 until field.h) {
            for (x in 0 until field.w) {
                output.print(field[Position(x, y)].toChar())
            }
            output.println()
        }
    }
}

interface CommandProcessor<P : Performer, E : EntityEssence> {
    fun getPerformer(command: String): EntityPerformer<E, P>?

    companion object {
        inline operator fun <P : Performer, E : EntityEssence> invoke(
                entity: Entity<E>,
                crossinline processCommand: (command: String) -> P?
        ) = object : CommandProcessor<P, E> {
            override fun getPerformer(command: String) = processCommand(command)?.let { EntityPerformer(entity, it) }
        }
    }
}

class ConsolePlayer<P : Performer, E : EntityEssence>(
    private val view: ConsoleView,
    private val commandProcessor: CommandProcessor<P, E>
) : Player<E, P> {
    override fun getPerformer() = wrap { commandProcessor.getPerformer(view.input.readLine()) }
}
