package test.lanterna

import com.googlecode.lanterna.input.KeyType
import model.engine.*
import test.*
import view.lanterna.*

private class TestLanternaEnvironment : TestEnvironment() {

    val view = LanternaView {
        if (entities.isEmpty()) {
            when (space) {
                "wall" -> '#'
                else -> '.'
            }
        } else '@'
    }

    init {
        LanternaPlayer(view, KeyStrokeProcessor(entity) { keyStroke ->
            when (keyStroke.keyType) {
                KeyType.Escape -> Exit(world)
                KeyType.ArrowUp -> entity.move(Direction.UP)
                KeyType.ArrowDown -> entity.move(Direction.DOWN)
                KeyType.ArrowLeft -> entity.move(Direction.LEFT)
                KeyType.ArrowRight -> entity.move(Direction.RIGHT)
                else -> null
            }
        }).getMoveMaker(world).register()
    }

    override fun run() {
        view.use {
            it.viewField(field)
            while (world.cycle()) {
                it.viewField(field)
            }
        }
    }
}

fun main() {
    TestLanternaEnvironment().run()
}
