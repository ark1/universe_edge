package test

import model.engine.*

import java.util.Random

private const val DENSITY = 0.1
private const val W = 20
private const val H = 20

abstract class TestEnvironment : Runnable {

    private val random = Random(0)

    protected val world = World()
    val field = Field(W, H, world) { x, y ->
        Cell(Position(x, y), if (x == 0 && y == 0 || random.nextDouble() > DENSITY) "air" else "wall")
    }

    val entity = Entity(Position(0, 0), field, Movable { direction ->
        when (field.getOrNull(position + direction)?.space) {
            "wall", null -> null
            else -> 1
        }
    }).apply { register() }
}
