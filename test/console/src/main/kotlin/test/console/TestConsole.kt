package test.console

import model.engine.*
import view.console.*
import test.*

private class TestConsoleEnvironment : TestEnvironment() {
    private val view = ConsoleView {
        if (entities.isEmpty()) {
            when (space) {
                "wall" -> '#'
                else -> '.'
            }
        } else '@'
    }

    init {
        ConsolePlayer(view, CommandProcessor(entity) { command ->
            when (command) {
                "x" -> Exit(world)
                "w" -> entity.move(Direction.UP)
                "s" -> entity.move(Direction.DOWN)
                "a" -> entity.move(Direction.LEFT)
                "d" -> entity.move(Direction.RIGHT)
                else -> null
            }
        }).getMoveMaker(world).register()
    }

    override fun run() {
        view.viewField(field)
        while (world.cycle()) {
            view.viewField(field)
        }
    }
}

fun main() {
    TestConsoleEnvironment().run()
}
