package model

import model.engine.*
import model.engine.Performer

interface AiState<out P : Performer> : PerformerProducer<P>
interface StateGetter<out P : Performer> {
    fun getState(): AiState<P>
    companion object {
        operator fun <P : Performer> invoke(getState: () -> AiState<P>) = object : StateGetter<P> {
            override fun getState() = getState()
        }
    }
}

class AiPerformerProducer<out P : Performer>(private val stateGetter: StateGetter<P>) : PerformerProducer<P> {
    override fun getPerformer() = stateGetter.getState().getPerformer()
}
