package model.engine

interface Performer {
    val duration: Int
    fun perform()

    companion object {
        inline operator fun invoke(duration: Int, crossinline perform: () -> Unit) = object : Performer {
            override val duration = duration
            override fun perform() = perform()
        }
    }
}

class Action<out P : Performer>(
        val world: World,
        private val performer: P
) : Performer {
    private var performed = false

    override val duration get() = performer.duration
    override fun perform() {
        if (performed) {
            error("This action is already performed")
        }
        performer.perform()
        performed = true
    }
}

class Exit(private val world: World) : Performer {
    override val duration = 0
    override fun perform() = world.close()
}

class EntityPerformer<out E : EntityEssence, out P : Performer>(
    private val entity: Entity<E>,
    private val performer: P
) : Performer {
    override val duration get() = performer.duration
    override fun perform() {
        if (entity.isFrozen) {
            error("The entity is still frozen")
        }
        performer.perform()
        entity.freeze(until = entity.world.time + duration)
    }
}

class Move(
    private val movable: Entity<Movable>,
    override val duration: Int,
    private val direction: Direction
) : Performer {
    override fun perform() {
        movable.cell.entities.remove(movable)
        movable.position += direction
        movable.cell.entities.add(movable)
    }
}

class DoNothing(
    private val entity: Entity<*>,
    override val duration: Int
) : Performer {
    override fun perform() {}
}
