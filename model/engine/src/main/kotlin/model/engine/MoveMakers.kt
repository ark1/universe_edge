package model.engine

inline fun <T : Any> wrap(body: () -> T?): T {
    var result: T?
    do {
        result = body()
    } while (result == null)
    return result
}

interface ActionProducer<out P : Performer> {
    fun getAction(): Action<P>
}

interface PerformerProducer<out P : Performer> {
    fun getPerformer(): P
    companion object {
        inline operator fun <P : Performer> invoke(crossinline getPerformer: () -> P) = object : PerformerProducer<P> {
            override fun getPerformer() = getPerformer()
        }
    }
}

class MoveMaker<out P : Performer>(
    private val world: World,
    time: Int? = null,
    private val priority: Int = 0,
    private val performerProducer: PerformerProducer<P>
) : Comparable<MoveMaker<*>>, ActionProducer<MoveMaker.MoveMakerPerformer<P>> {
    private val id = world.generateMoveMakerId()

    var time = time ?: world.time
        private set

    override fun compareTo(other: MoveMaker<*>) = compareBy<MoveMaker<*>> (
            { it.time },
            { -it.priority },
            { it.id }
    ).compare(this, other)

    fun register() = world.push(this)

    class MoveMakerPerformer<out P : Performer>(
            private val moveMaker: MoveMaker<P>,
            private val performer: P
    ) : Performer {
        override val duration get() = performer.duration
        override fun perform() {
            performer.perform()
            moveMaker.time += duration
        }
    }

    override fun getAction(): Action<MoveMakerPerformer<P>> {
        return Action(world, MoveMakerPerformer(this, performerProducer.getPerformer()))
    }
}

fun <P : Performer> PerformerProducer<P>.getMoveMaker(world: World, time: Int? = null, priority: Int = 0)
        = MoveMaker(world, time, priority, this)

interface Player<out E : EntityEssence, out P : Performer> : PerformerProducer<EntityPerformer<E, P>>
