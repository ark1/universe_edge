package model.engine

data class Position(
    val x: Int,
    val y: Int
) {
    operator fun minus(other: Position) = Direction(x - other.x, y - other.y)
    operator fun plus(direction: Direction) = Position(x + direction.dx, y + direction.dy)
    operator fun minus(direction: Direction) = Position(x - direction.dx, y - direction.dy)
}

data class Direction(
    val dx: Int,
    val dy: Int
) {
    operator fun plus(other: Direction) = Direction(dx + other.dx, dy + other.dy)
    operator fun minus(other: Direction) = Direction(dx - other.dx, dy - other.dy)
    operator fun unaryMinus() = Direction(-dx, -dy)

    operator fun plus(position: Position) = position + this

    companion object {
        val LEFT = Direction(-1, 0)
        val RIGHT = Direction(1, 0)
        val UP = Direction(0, -1)
        val DOWN = Direction(0, 1)

        val STAY = Direction(0, 0)

        fun getAll() = sequence {
            yield(STAY)

            var distance = 1
            while (true) {
                val range = -distance until distance
                yieldAll(range.asSequence().map { Direction(distance, it) })
                yieldAll(range.asSequence().map { Direction(-it, distance) })
                yieldAll(range.asSequence().map { Direction(-distance, -it) })
                yieldAll(range.asSequence().map { Direction(distance, it) })
                distance++
            }
        }.asIterable()

        fun getAllCloserThan(maxDistance: Int) = getAll().take((maxDistance * 2 + 1) * (maxDistance * 2 + 1))
    }
}