package model.engine

import kotlin.math.max

interface MovingChecker {
    fun getMovingTime(entity: Entity<Movable>, direction: Direction): Int?
    companion object {
        inline operator fun invoke(crossinline getMovingTime: Entity<Movable>.(Direction) -> Int?) = object : MovingChecker {
            override fun getMovingTime(entity: Entity<Movable>, direction: Direction) = entity.getMovingTime(direction)
        }
    }
}

interface EntityEssence

interface Movable : EntityEssence {
    fun move(entity: Entity<Movable>, direction: Direction): Move?

    companion object {
        operator fun invoke(movingChecker: MovingChecker) = object : Movable {
            override fun move(entity: Entity<Movable>, direction: Direction): Move? {
                return if (entity.isFrozen) null else
                    return Move(
                        entity, movingChecker.getMovingTime(entity, direction) ?: return null, direction
                    )
            }
        }

        inline operator fun invoke(crossinline getMovingTime: Entity<Movable>.(Direction) -> Int?) = object : Movable {
            override fun move(entity: Entity<Movable>, direction: Direction): Move? {
                return if (entity.isFrozen) null else
                    return Move(
                        entity, entity.getMovingTime(direction) ?: return null, direction
                    )
            }
        }
    }
}
fun Entity<Movable>.move(direction: Direction) = essence.move(this, direction)

class Entity<out E : EntityEssence>(
    var position: Position,
    var field: Field,
    val essence: E
) {
    private var frozenUntil = 0

    val world get() = field.world
    private val currentTime get() = world.time
    val cell get() = field[position]

    fun freeze(until: Int) {
        frozenUntil = max(until, frozenUntil)
    }

    val isFrozen get() = frozenUntil > currentTime

    fun register() {
        field[position].entities += this
    }
}
