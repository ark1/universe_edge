package model.engine

import java.io.Closeable
import java.util.PriorityQueue

class World : Closeable {

    var time = 0
        private set

    private val queue = PriorityQueue<MoveMaker<*>>()
    fun push(moveMaker: MoveMaker<*>) {
        queue.add(moveMaker)
    }

    private var closed = false
    override fun close() {
        closed = true
    }

    fun cycle(): Boolean {
        if (closed or queue.isEmpty()) {
            return false
        }

        val moveMaker = queue.remove()
        time = moveMaker.time

        var action: Action<*>? = null
        while (action == null) {
            action = moveMaker.getAction()
        }

        action.perform()
        push(moveMaker)
        return true
    }

    private var moveMakerCounter = 0L
    fun generateMoveMakerId() = moveMakerCounter++
}