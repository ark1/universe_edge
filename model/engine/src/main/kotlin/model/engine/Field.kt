package model.engine

class Cell(
    val position: Position,
    val space: String,
    entities: Iterable<Entity<*>>? = null
) {
    val entities = entities?.toMutableSet() ?: mutableSetOf()
}

class Field(
    val w: Int,
    val h: Int,
    val world: World,
    init: (Int, Int) -> Cell
) : Map<Position, Cell> {

    override val entries: Set<Map.Entry<Position, Cell>>
        get() = this.field.flatMap { it.map { cell ->
            object : Map.Entry<Position, Cell> {
                override val key = cell.position
                override val value = cell
            }
        }}.toSet()

    override val keys: Set<Position>
        get() = this.field.flatMap { it.map(Cell::position) }.toSet()

    override val size: Int
        get() = w * h
    override val values: Collection<Cell>
        get() = this.field.flatten()

    override fun containsKey(key: Position) = key.x >= 0 && key.y >= 0 && key.x < w && key.y < h

    override fun containsValue(value: Cell) = values.contains(value)

    override fun isEmpty() = size == 0

    private val field = MutableList(w) { x ->
        MutableList(h) { y ->
            init(x, y)
        }
    }

    override operator fun get(key: Position) = field[key.x][key.y]
    operator fun set(key: Position, value: Cell) {
        field[key.x][key.y] = value
    }

    fun getOrNull(key: Position) = if (containsKey(key)) this[key] else null
}